//
//  main.m
//  TestProject
//
//  Created by SnowMan on 2016-05-30.
//  Copyright © 2016 Matej. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
