//
//  AppDelegate.h
//  TestProject
//
//  Created by SnowMan on 2016-05-30.
//  Copyright © 2016 Matej. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

